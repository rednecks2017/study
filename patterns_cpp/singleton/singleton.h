/**
 * @brief Реализация паттерна "одиночка" (Singleton)
 */
template <class T>
class Singleton {
   public:
    static T *inst() {
        static T inst;
        return inst;
    }
};