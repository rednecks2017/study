/**
 * @file CleverPnt.cpp
 * @author ball
 */

#include <iostream>
#include "Resource\ResourceManager.h"
#include "Resource\Resource.h"

ResourceManager resManager;

std::shared_ptr<Resource> f1() {
	std::cout << "f1 begin" << std::endl;

	auto testResource = resManager.get("testTextFile.txt");
	std::cout << testResource->get() << std::endl;

	auto testResource1 = resManager.get("testTextFile1.txt");
	std::cout << testResource1->get() << std::endl;

	auto testResource2 = resManager.get("testTextFile2.txt");
	std::cout << testResource2->get() << std::endl;

	std::cout << "f1 return" << std::endl;
	return testResource2;
}

void f2(std::shared_ptr<Resource> resource2) {
	std::cout << "f2 begin" << std::endl;
	
	auto resource = resManager.get("testTextFile.txt");
	auto anotherResource2 = resManager.get("testTextFile2.txt");

	std::cout << "f2 return" << std::endl;
}

int main()
{
	auto resource2 = f1();
	f2(resource2);

	int someNum;
	std::cin >> someNum;
    return 0;
}

