/**
* @author ball
*/

#include <fstream>
#include <iostream>
#include "Resource.h"

Resource::Resource(const std::string& filePath) {
	loadFromFile(filePath);
}

Resource::~Resource() {
	std::cout << "!!! Free resource !!!" << std::endl;
}

const std::string& Resource::get() const {
	return const_cast<const std::string&>(str);
}

void Resource::loadFromFile(const std::string& filePath) {
	std::cout << "!!! Load resource " << filePath << " !!!" << std::endl;
	str.clear();
	try {
		std::fstream fs(filePath, std::fstream::in);
        if (fs.good()) {
			fs.seekg(0, fs.end);
			str.reserve(static_cast<const size_t>(fs.tellg()));
			fs.seekg(0, fs.beg);

			str.assign(std::istreambuf_iterator<char>(fs), std::istreambuf_iterator<char>());
        }
	}
	catch (...) {
		str.clear();
	}
}