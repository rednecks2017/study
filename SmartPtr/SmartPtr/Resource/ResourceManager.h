#ifndef RESOURCE_MANAGER_H
#define RESOURCE_MANAGER_H
/**
 * @author ball
 */

#include <map>
#include <memory>

class Resource;

/**
 * @brief Manager of the resources
 * Implement loading resource from file during the first call. In case nobody doesn't use resource,
 * the memory and the resource would be free by shared_ptr automaticly.
 * During the new call to resource, it would be reload.
 */
class ResourceManager {
	typedef std::map<std::string, std::weak_ptr<Resource>> ResourceMap;
public:
	ResourceManager();

	/**
	 * @brief Getting shared_ptr to the resource loading from file 
	 */
	std::shared_ptr<Resource> get(const std::string &filePath);
private:
	// std::unique_ptr is used here only for practice whith its using. Threre is no any reasons.
	// In fact we can just define an instance of ResourceMap here.
	std::unique_ptr<ResourceMap> resourcePtr; //!< pointer to map of the resources

	/**
	 * @brief Load resource from file and give all rights of it to the new owner
	 */
	static std::unique_ptr<Resource> loadFromFile(const std::string &filePath);
};

#endif // RESOURCE_MANAGER_H