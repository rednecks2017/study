#ifndef RESOURCE_H
#define RESOURCE_H
/**
 * @author ball
 */

#include <string>

class ResourceManager;

/**
 * @brief Wrap for data (resource)
 */
class Resource {
	friend class ResourceManager;
public:
	/**
	 * @brief Getting internal data
	 */
	const std::string& get() const;

	~Resource();
private:
	Resource(const std::string& filePath);

	std::string str; //!< just text

	/**
	 * @brief Loading resource from file
	 */
	void loadFromFile(const std::string& filePath);
};

#endif // RESOURCE_H