/**
 * @author ball
 */

#include "ResourceManager.h"
#include "Resource.h"

ResourceManager::ResourceManager():
	resourcePtr(new ResourceMap) {
}

std::shared_ptr<Resource> ResourceManager::get(const std::string &filePath) {
	ResourceMap::iterator itr = resourcePtr->find(filePath);
	// In case the resource is new or nobody is using it now, load it from file
	if (itr == resourcePtr->end() || itr->second.expired()) {
		std::shared_ptr<Resource> shrd_ptr(loadFromFile(filePath));
		(*resourcePtr)[filePath] = shrd_ptr;
		return shrd_ptr;
	}
	// Else return pointer to already loaded resource
	return itr->second.lock();
}

std::unique_ptr<Resource> ResourceManager::loadFromFile(const std::string &filePath) {
	std::unique_ptr<Resource> resourcePtr(new Resource(filePath));
	return resourcePtr;
}