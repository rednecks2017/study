/**
* @author ball
* @brief Task for reverting list
*/

#ifndef REVERT_LIST_H
#define REVERT_LIST_H

/**
 * My forward list realization
 */
template<typename T>
class ForwardList {
private:
	class Node {
	public:
		Node(Node *nxt, const T val):
			next(nxt),
			value(val) {}

		Node *next;
		T value;
	};
public:
	ForwardList() :
		_first(nullptr),
		_size(0) {
	}

	~ForwardList() {
		while (_size) {
			popFront();
		}
	}

	T& front() {
		return *_first.value;
	}

	void pushFront(T value) {
		Node *newNode = new Node(_first, value);
		Node *next = _first;
		_first = newNode;
		_first->next = next;
		++_size;
	}

	void popFront() {
		if (!_size) {
			return;
		}
		Node *next = _first->next;
		delete _first;
		_first = next;
		--_size;

	}

	size_t size() {
		return _size;
	}

	void printElements() {
		Node *node = _first;
		for (size_t i = 0; i < _size; ++i) {
			std::cout << node->value << "\t";
			node = node->next;
		}
		std::cout << std::endl;
	}

	void reverse() {
		Node *prev = nullptr;
		Node *curr = _first;
		Node *next = nullptr;
		for (size_t i = 0; i < _size; ++i) {
			next = curr->next;
			curr->next = prev;

			prev = curr;
			curr = next;
		}
		_first = prev;
	}
private:
	Node *_first;
	size_t _size;
};

#endif // REVERT_LIST_H