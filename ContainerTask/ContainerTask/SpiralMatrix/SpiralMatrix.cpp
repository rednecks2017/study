/**
* @author ball
*/

#include <iostream>
#include "SpiralMatrix.h"

Matrix::Matrix(const size_t w_2):
	mySize(2 * w_2 + 1),
	m(mySize, Row(mySize)) {
}

void Matrix::printMatrix() {
	for (auto i : m) {
		for (auto j : i.row) {
			std::cout << j << "\t";
		}
		std::cout << std::endl << std::endl;
	}
}

size_t Matrix::size() {
	return mySize;
}

///////////////////////////////////////////////////////////////

/**
 * @brief Set one spiral arm from the upper right element
 * m[i][j] = rightUpVal
 * then going clockwise down till m[i][j-1]
 * then go to the next spiral arm m[i-1][j-1] (make recursion)
 */
static void Set�ircleOfMatrix(Matrix& m, const int i, const int j, const int rightUpVal) {
	m[i][j] = rightUpVal;
	if (i == j) {
		return;
	}
	const int size = m.size() - 2 * i;
	// Set right column
	for (int i1 = i + 1; i1 < i + size; ++i1) {
		m[i1][j] = m[i1 - 1][j] - 1;
	}
	// Set down row
	for (int j1 = j - 1; j1 >= static_cast<int>(m.size()) - j - 1; --j1) {
		m[i + size - 1][j1] = m[i + size - 1][j1 + 1] - 1;
	}
	// Set left column
	for (int i2 = i + size - 2; i2 >= i; --i2) {
		m[i2][m.size() - j - 1] = m[i2 + 1][m.size() - j - 1] - 1;
	}
	// Set upper row
	for (int j2 = m.size() - j; j2 < j; j2++) {
		m[i][j2] = m[i][j2 - 1] - 1;
	}
	// Go to the next spiral arm
	Set�ircleOfMatrix(m, i + 1, j - 1, m[i][j - 1] - 1);
}

void MakeSpiralMatrix(Matrix& m, const int centralValue) {
	size_t size = m.size();
	Set�ircleOfMatrix(m, 0, size - 1, centralValue + size * size - 1);
}