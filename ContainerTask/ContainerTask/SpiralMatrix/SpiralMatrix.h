/**
* @author ball
*/

#ifndef SPIRAL_MATRIX_H
#define SPIRAL_MATRIX_H

#include <vector>

//! Matrix realization
class Matrix {
	class Row;
public:
	Matrix(const size_t w_2);

	//! Print the content of the matrix to the screen
	void printMatrix();

	//! Get size of the matrix
	size_t size();

	Row& operator[](size_t i) {
		return m[i];
	}

private:
	const size_t mySize; //!< size of the matrix
	std::vector<Row> m; //!< matrix

	//! matrix row
	class Row {
		friend class Matrix;
	public:
		Row(size_t size):
		row(size) {
		}

		int& operator[](size_t j) {
			return row[j];
		}
	private:
		std::vector<int> row; //!< row
	};
};

//////////////////////////////////////////////////////////////

/**
 * @brief Make given matrix sprial with centerValue in center
 */
void MakeSpiralMatrix(Matrix& matrix, const int centralValue);

#endif // SPIRAL_MATRIX_H