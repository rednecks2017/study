/**
 * @author ball
 */

#include <iostream>
#include <thread>
#include <forward_list>

#include "RevertList\RevertList.h"
#include "SpiralMatrix\SpiralMatrix.h"
#include "Clock\Timer.h"

void PushAnyKey() {
	int someNum;
	std::cout << std::endl << "Press any key" << std::endl;
	std::cin >> someNum;
}

void f(Matrix* m, const int centralValue) {
	MakeSpiralMatrix(*m, centralValue);
}

int main()
{
	// Matrix
	Matrix m(4);
	std::thread first(f, &m, 0);
	first.join();
	m.printMatrix();

	// Reverse forwardList
	const int maxElements = 1000000;
	ForwardList<int> myForwList;
	for (int i = 0; i < maxElements; ++i) {
		myForwList.pushFront(i);
	}
	Timer timer;
	timer.start();
	myForwList.reverse();
	timer.stop();
	std::cout << "MyForwardList reverse time = " << timer.getMicrosec() << std::endl;

	std::forward_list<int> forwList;
	for (int i = 0; i < maxElements; ++i) {
		forwList.push_front(i);
	}
	timer.reset();
	timer.start();
	forwList.reverse();
	timer.stop();
	std::cout << "std::forward_list reverse time = " << timer.getMicrosec() << std::endl;

	PushAnyKey();
    return 0;
}

