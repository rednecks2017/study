/**
* @author ball
*/

#include "Timer.h"
#include "ClockUtil.h"

Timer::Timer() :
timerIsTurnedOn(false),
currentValueNanosec(0),
previousTimeNanosec(GetNanosec()) {
}

uint64_t Timer::getNanosec() {
	std::lock_guard<std::mutex> locker(mtx);
	return getCurrentValueNanosec();
}

uint64_t Timer::getMicrosec() {
	std::lock_guard<std::mutex> locker(mtx);
	return getCurrentValueNanosec() / 1000;
}

uint64_t Timer::getMilisec() {
	std::lock_guard<std::mutex> locker(mtx);
	return getCurrentValueNanosec() / 1000000;
}

bool Timer::isGoing() {
	std::lock_guard<std::mutex> locker(mtx);
	return timerIsTurnedOn;
}

void Timer::start() {
	std::lock_guard<std::mutex> locker(mtx);
	if (timerIsTurnedOn) {
		return;
	}

	previousTimeNanosec = GetNanosec();
	timerIsTurnedOn = true;
}

void Timer::stop() {
	std::lock_guard<std::mutex> locker(mtx);
	if (!timerIsTurnedOn) {
		return;
	}

	getCurrentValueNanosec();
	timerIsTurnedOn = false;
}

void Timer::reset() {
	std::lock_guard<std::mutex> locker(mtx);
	previousTimeNanosec = GetNanosec();
	currentValueNanosec = 0;
}

uint64_t Timer::getCurrentValueNanosec() {
	if (timerIsTurnedOn) {
		currentValueNanosec += GetNanosec() - previousTimeNanosec;
		previousTimeNanosec = GetNanosec();
	}
	return currentValueNanosec;
}