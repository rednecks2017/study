/** 
 * @file ClockUtil.cpp
 * @author ball
 * @date March 15, 2017
 */

#include <chrono>

template<typename T>
static uint64_t GetChronoTime() {
	const auto timePoint = std::chrono::high_resolution_clock::now();
	const auto duration = timePoint.time_since_epoch();
	return std::chrono::duration_cast<T>(duration).count();
}

uint64_t GetMillisec() {
	return GetChronoTime<std::chrono::milliseconds>();
}

uint64_t GetMicrosec() {
	return GetChronoTime<std::chrono::microseconds>();
}

uint64_t GetNanosec() {
	return GetChronoTime<std::chrono::nanoseconds>();
}