/** 
 * @file ClockUtil.h
 * @author ball
 * @date March 15, 2017
 */

#ifndef CLOCKUTIL_H
#define CLOCKUTIL_H

#include <stdint.h>

//! Получение времени в милисекундах (начиная от 1970 года)
uint64_t GetMillisec();
//! Получение времени в микросекундах (начиная от 1970 года)
uint64_t GetMicrosec();
//! Получение времени в наносекундах (начиная от 1970 года)
uint64_t GetNanosec();

#endif /* CLOCKUTIL_H */