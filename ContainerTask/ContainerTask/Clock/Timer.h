/**
 * @author ball
 */

#ifndef TIMER_H
#define TIMER_H

#include <stdint.h>
#include <mutex>

/**
 * @brief Timer
 * This timer is safety for using from different threads
 */
class Timer {
public:
	Timer();

	// Getting timer
	uint64_t getNanosec();
	uint64_t getMicrosec();
	uint64_t getMilisec();

	/**
	 * @brief Check if timer is going
	 */
	bool isGoing();

	/**
	 * @brief Start timer
	 */
	void start();

	/**
	 * @brief Stop timer
	 */
	void stop();

	/**
	 * @brief Reset timer srtart time
	 */
	void reset();

private:
	std::mutex mtx; //!< mutex
	bool timerIsTurnedOn; //!< true - if timer is going
	uint64_t currentValueNanosec;  //!< current value of the timer (calculated only if needed)
	uint64_t previousTimeNanosec; //!< previous absolut time

	/**
	 * @brief Get current value of the timer
	 */
	uint64_t getCurrentValueNanosec();
};

#endif // TIMER_H